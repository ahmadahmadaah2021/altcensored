��    9      �              �     �     �     �     �     �     �     �             �         �  0   �     �  -   �     '     0     8     @     E  	   J     T     a     t     y     �     �     �     �     �     �  !   �     �     �  &   �  �     8   �     �     �            #        C  ?   J     �     �     �     �     �     �     �     �     �     �     �       Z     �  t  #   
  	   &
     0
     6
     ?
     E
     ]
  	   d
     n
  �   }
       ;   &     b  7   f     �  
   �  
   �     �     �  
   �     �     �     �  
   �     �                         "  4   '     \     `     w  �   �  D   A     �     �     �     �  '   �     �  E   �     4     =     X     h     o     �     �     �     �     �     �     �  U   �   "Limited state" videos About Archive Category Channel Community Guidelines Created Deleted Download Torrent Download videos with "right mouse button click" inside the video window or use the "Download Torrent" link. Channel and Video RSS Feeds Email new channel suggestions to Embed videos in any website exactly like YouTube False Find videos and channels using the search bar Language Limited Monitor None Part Published Published on Released under the Save Subscribers Terms and Conditions Total True Videos View on Views access to videos that are neither and arbitrarily enforced are being archived in case of deletion are placed behind a warning message, cannot be shared, monetized or easily found, and have likes, comments, view counts and suggested videos disabled contain torrent links to easily download multiple videos deleted ever-changing have been deleted and illegal is an unbiased community catalog of latest limited state, removed, and self-censored YouTube videos across limits monitored channels, of which most limited newest nor violate their own oldest or popular removes search videos that violate their views while Creators increasingly "self-censor" trying to avoid channel strikes and terminations Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-11-02 19:39-0300
PO-Revision-Date: 2020-07-28 20:05+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: se
Language-Team: se <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 Videoklipp för "begränsad status" Handla om Arkiv Kategori Kanal Gemenskapens riktlinjer Skapad Borttagen Hämta Torrent Hämta videoklipp med "högermusknappen klicka" i videofönstret eller använd länken "Hämta Torrent". RSS-feeds för kanaler och video E-posta nya kanalförslag till Embed in videoklipp på alla webbplatser precis som YouTube Fel Sök efter videor och kanaler med hjälp av sökfältet Språk Begränsad Bildskärm Ingen Del Publicerad Publicerad den Släppt under Spara Abonnenter Villkor Totalt Sant Videor Visa på Vyer tillgång till videoklipp som inte är något av dem och godtyckligt påtvingad arkiveras om den tas bort placeras bakom ett varningsmeddelande, inte kan delas, användas i pengar eller enkelt att hitta och ha favoriter, kommentarer, vyantal och föreslagna videoklipp inaktiverade innehåller tortyrlänkar för att enkelt ladda ned flera videoklipp utgår ständigt föränderlig har strukits och olaglig är en objektiv gemenskapskatalog över senaste begränsade, borttagna och självcensurerade YouTube-videoklipp över gränser övervakade kanaler, varav mest begränsad nyaste inte kränka deras egen äldst eller populär tar bort Sök videofilmer som kränker deras vyer medan Skapare alltmer "självcensor" försöker undvika kanal strejker och avslutande 