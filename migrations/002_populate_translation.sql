INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab1', 'video', 'video', 'vídeo', 'vidéo', 'vídeo', 'video', 'video', 'video');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab2', 'channel', 'kanal', 'canal', 'canal', 'canal', 'kanaal', 'canale', 'kanal');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab3', 'playlist', 'playlist', 'playlist', 'playlist', 'playlist', 'afspeellijst', 'playlist', 'spellista');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab4', 'category', 'kategorie', 'categoría', 'catégorie', 'categoria', 'categorie', 'categoria', 'kategori');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab5', 'settings', 'einstellungen', 'ajustes', 'paramètres', 'configurações', 'instellingen', 'impostazioni', 'inställningar');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab6', 'user', 'benutzer', 'usuario', 'usager', 'usuário', 'gebruiker', 'utente', 'användare');
INSERT INTO "translation"("varname", "en", "de", "es", "fr", "pt", "nl", "it", "se") VALUES ('navtab7', 'language', 'sprachen', 'idiomas', 'langues', 'idiomas', 'sprog', 'lingue', 'språk');
