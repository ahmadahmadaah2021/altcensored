INSERT INTO "public"."translation" VALUES ('navtab1', 'video', 'video', 'vídeo', 'vidéo', 'vídeo', 'video', 'video', 'video');
INSERT INTO "public"."translation" VALUES ('navtab2', 'channel', 'kanal', 'canal', 'canal', 'canal', 'kanaal', 'canale', 'kanal');
INSERT INTO "public"."translation" VALUES ('navtab3', 'category', 'kategorie', 'categoría', 'catégorie', 'categoria', 'categorie', 'categoria', 'kategori');
INSERT INTO "public"."translation" VALUES ('navtab5', 'settings', 'einstellungen', 'ajustes', 'paramètres', 'configurações', 'instellingen', 'impostazioni', 'inställningar');
INSERT INTO "public"."translation" VALUES ('navtab4', 'playlist', 'playlist', 'playlist', 'playlist', 'playlist', 'afspeellijst', 'playlist', 'spellista');
INSERT INTO "public"."translation" VALUES ('navtab6', 'user', 'benutzer', 'usuario', 'usager', 'usuário', 'gebruiker', 'utente', 'användare');